# HTTP server for IPK course

Implementation of simple HTTP server in Python 3 using sockets API.
Server does not support concurrent request processing.

## How to run

Since the server is implemented in Python 3 which is an interpreted
language no compilation is needed. Just make sure you have python3 installed
on your system. It's present by default on most Linux distributions

### Using the server

To start the server type in the terminal:

> ./ipkhttpserver

This will run the server on port 80 without chunked transfer encoding. 
You can specify server port using `-p` or `--port` parameter:

> ./ipkhttpserver -p 8000

If you specify chunk size using `-c` or `--chunk-max-size` parameter the server will
use chunked transfer encoding if response body size will be greater than
this value. Chunk size will be fixed and set to this value (with the exception
of two last chunks):

> ./ipkhttpserver --port=8080 --chunk-max-size=50

You can also specify the minimal time between chunks in miliseconds using `-t`
or `--min-chunk-time` argument. This setting has no effect if `-c`/`--chunk-max-size`
argument is not present or set to `0`

> ./ipkhttpserver --port=8080 --chunk-max-size=50 -t 50

The `-h` or `--help` parameter will show explanation of all the parameters 
above and some other information about the program.

### Using the client

I have prepared a simple http client which sends a request to the given URI
and saves the response into a file

> ./ipkhttpklient http://localhost:8000/dir/

Response header will be saved as .header file and reconstructed body as .payload
file. Client was also tested on recieving some static content from Apache web 
server.

## Protocol support

This HTTP server supports a subset of HTTP 1.1 features:

- GET method
- Chunked Transfer Encoding with fixed chunk size
- Content type: text/plain

### Reponse codes

- 200 - OK
- 403 - FORBIDDEN
- 404 - NOT FOUND
- 501 - NOT IMPLEMENTED

### Response body

- if not using GET method: `ERROR 501: PLEASE USE GET METHOD`
- if requested path is not found: `ERROR 404: REQUESTED FILE OR DIR NOT FOUND`
- if access to requested path is forbidden: `ERROR 403: FORBIDDEN ACCESS`
- if file: shows file contents
- if directory: shows list of directory items

## Implementation

### src/config.py

Contains mostly string constants for example non-200 OK HTTP response bodies,
or log filenames

### src/log.py

Contains class to simplify logging process so I don't have to mannualy generate
timestamp and repeat open-write-close process

### src/request.py

Parses HTTP request bytes recieved through socket from client and reconstructs
important information needed to handle the request
(requested path, method and HTTP version)

### src/responsebody.py

Creates response body as utf-8 string. If requested path refers to a file, the
body will be the contents of this file. If requested path refers to a directory
the response will be a list of directory items (files or subdirectories)

Throws exceptions if file/directory is not found or cannot be opened for reading

### src/response.py

Contains a class which takes response body and transforms it into full HTTP
response ready to be sent to client

### src/test.py

Runs unittests which test all the modules above. If you want to test, just type

> python3 src/test.py

into the terminal

### src/client.py

Makes HTTP GET request to the given URL and returns respons headers and body

### src/server.py

Starts the HTTP server implemented as infinite loop which listens to client
requests then parses it, builds response body, builds full response and sends it
to the client

### ipkhttpclient and ipkhttpserver

These files are just wrappers for src/client.py and src/server.py which just allow
to configure client and server using command line arguments and save response recieved
by client to files

