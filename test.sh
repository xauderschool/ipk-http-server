#!/usr/bin/env bash
echo "--------------------------------------------"

# Tests for the server and client as whole programs

echo "Starting the server ..."
echo "Chunk size 10, chunk delay 10ms"

./ipkhttpserver -c 10 -p 8000 -t 10 &
SERVER_PID=$!

sleep 2


# Test 1: Directory content
echo "--------------------------------------------"
echo "TEST 01: Testing getting directory:"
./ipkhttpclient http://localhost:8000/dir
echo 
echo "Expected"
echo "--------"
echo "DIR"
echo "- test2.txt"
echo "- test.txt"
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo "Headers"
echo "-------"
cat *.header

rm *.payload
rm *.header


# Test 2: File not found
echo "--------------------------------------------"
echo "TEST 02: Testing getting non existing file:"
./ipkhttpclient http://localhost:8000/dir/not_existing
echo 
echo "Expected"
echo "--------"
echo "ERROR 404: REQUESTED FILE OR DIR NOT FOUND"
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo
echo "Headers"
echo "-------"
cat *.header

rm *.payload
rm *.header


# Test 3: File contents
echo "--------------------------------------------"
echo "TEST 03: Testing getting file contents with utf-8:"
./ipkhttpclient http://localhost:8000/dir/test2.txt
echo 
echo "Expected"
echo "--------"
echo "tento súbor obsahuje nejaké nepekné znaky, ktoré nenájdeš v ASCII"
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo
echo "Headers"
echo "-------"
cat *.header

rm *.payload
rm *.header


# Test 4: File contents
echo "--------------------------------------------"
echo "TEST 04: Testing getting file contents:"
./ipkhttpclient http://localhost:8000/dir/test.txt
echo 
echo "Expected"
echo "--------"
echo "some content of file is here"
echo
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo
echo "Headers"
echo "-------"
cat *.header

rm *.payload
rm *.header

# Test 5: File contents
echo "--------------------------------------------"
echo "TEST 05:Testing getting small file:"
./ipkhttpclient http://localhost:8000/small.txt
echo 
echo "Expected"
echo "--------"
echo "abcs"
echo
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo
echo "Headers"
echo "-------"
cat *.header

rm *.payload
rm *.header

# Test 6: File forbidden
echo "--------------------------------------------"
echo "TEST 06 Testing getting forbidden file contents:"
echo
echo "Changing chmod of ./WWW/dir/test.txt"
chmod a-r WWW/dir/test.txt

./ipkhttpclient http://localhost:8000/dir/test.txt
echo 
echo "Expected"
echo "--------"
echo "ERROR 403: FORBIDDEN ACCESS"
echo
echo 
echo "Returned"
echo "--------"
cat *.payload
echo
echo
echo "Headers"
echo "-------"
cat *.header
echo
echo "Changing chmod of ./WWW/dir/test.txt"
chmod a+r WWW/dir/test.txt

rm *.payload
rm *.header

# No more tests 
echo "--------------------------------------------"
echo "Stopping the server ..."
kill ${SERVER_PID}

echo "You can check generated log files for detailed communication"
echo "between the server and the client"
