#!/usr/bin/env python3
import datetime
from config import STATUS_TEXTS


class Response(object):
	"""
	Generates response which is ready to be sent to client using sockets API

	If response is split into chunks, use get_headers(), get_next_chunk() and
	get_next_chunk_size() methods

	If chunked transfer encoding is not used (chunk size is 0 or grater than
	body length) just use get_whole() method
	"""

	def __init__(self, status_code, body, chunk_size):
		"""
		Generated response depends on status code, response body and chunk size.
		"""

		self.use_chunks = chunk_size < len(bytes(body, "utf-8")) and chunk_size != 0

		if not self.use_chunks:
			self.body = body
		else:
			self.make_chunks(body, chunk_size)
			self.current_chunk_index = 0
			self.current_size_index = 0

		self.headers = self.make_headers(status_code)

	def get_body(self):
		"""
		Returns whole response body if chunks are not used, which can be sent to 
		client using sockets API without any modifications
		"""
		return bytes(self.body, "utf-8")

	def get_headers(self):
		"""
		Returns response headers without response body or chunks. This should be
		used when response is split into chunks to send the header to client
		"""
		return bytes(self.headers, "utf-8")

	def get_next_chunk(self):
		"""
		Returns following chunk contents ready to be sent to client
		First call returns first chunk, second call returns second chunk etc.

		Use reset_indexes() method to start from first chunk again

		Throws: IndexError when no more chunks can be returned
		"""
		chunk_bytes = bytearray(self.chunks[self.current_chunk_index])
		chunk_bytes.append(13)
		chunk_bytes.append(10)
		self.current_chunk_index += 1
		return bytes(chunk_bytes)

	def get_next_chunk_size(self):
		"""
		Returns following chunk size ready to be sent to client
		First call returns first chunk size, second call returns second chunk size etc.
		The counter is independent from get_next_chunk() method

		Use reset_indexes() method to start from first chunk again

		Throws: IndexError when no more chunk sizes can be returned
		"""
		chunk = self.chunks[self.current_size_index]
		chunk_size_int = len(chunk)
		chunk_size_hex = hex(chunk_size_int)[2:] + "\r\n"
		chunk_size_bytes = bytes(chunk_size_hex, "utf-8")
		self.current_size_index += 1
		return chunk_size_bytes

	def is_using_chunks(self):
		"""
		Returns True if response uses chunked transer encoding. This happens
		when chunk size set in __init__ method is not 0 and body size is greater
		than this chunk size
		"""
		return self.use_chunks

	def reset_indexes(self):
		"""
		Resets indexes to access chunks when using get_next_chunk() and 
		get_next_chunk_size() methods so the next call of these methods returns
		contents/size of first chunk again
		"""
		self.current_chunk_index = 0
		self.current_size_index = 0

	def make_chunks(self, body, chunk_size):
		"""
		Splits response body into chunks. Empty chunk is also appended as
		specified in HTTP/1.1 specification
		"""
		self.chunks = list()
		body = bytes(body, "utf-8")

		while len(body) > 0:
			chunk = body[:chunk_size]
			body = body[chunk_size:]
			self.chunks.append(chunk)

		self.chunks.append(b"")

	def make_headers(self, status_code):
		"""
		Creates correct headers depending on status code and whether the response
		is using chunked transfer encoding or not
		"""
		headers  = "HTTP/1.1 %d %s\r\n" % (status_code, STATUS_TEXTS[status_code])
		headers += "Connection: close\r\n"
		headers += "Date: %s\r\n" % datetime.datetime.now().isoformat()
		headers += "Server: xsvana01/1.0.0\r\n"
		headers += "Content-Type: text/plain\r\n"

		if self.use_chunks:
			headers += "Transfer-Encoding: chunked\r\n"
		else:
			headers += "Content-Length: %d\r\n" % len(bytes(self.body, "utf-8"))

		headers += "\r\n"
		return headers
