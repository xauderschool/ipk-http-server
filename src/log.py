#!/usr/bin/env python3
from datetime import datetime

class Log(object):

	def __init__(self, basic_filename, suffix):
		"""
		Creates and opens log file to writing

		Param base_filename: basic log filename
		Param suffix: log file suffix

		Full log filename will be <base_filename>-<current_timestamp>.<suffix>
		"""
		self.filename = self.make_full_filename(basic_filename, suffix)

	def write_message(self, message):
		"""
		Writes current timestamp and message to log file
		Param message: string to write into the log
		"""
		message = "%s\n\n" % message
		timestamp = "%s\n\n" % datetime.now().isoformat()
		file_descriptor = open(self.filename, "a+")
		file_descriptor.write(timestamp)
		file_descriptor.write(message)
		file_descriptor.close()

	def make_full_filename(self, base_filename, suffix):
		"""
		Log files have to contain timestamp so we build the whole filename
		using basename, current timestamp and suffix

		Param base_filename: basic log filename
		Param suffix log file suffix

		Returns: log filename: <base_filename>-<current_timestamp>.<suffix>
		"""
		date_now = datetime.now()
		suffix = ".%s" % suffix if suffix[0] != "." else suffix
		timestamp = date_now.strftime("%Y-%m-%d:%H:%M:%S")
		full_filename = "%s-%s%s" % (base_filename, timestamp, suffix)
		return full_filename

