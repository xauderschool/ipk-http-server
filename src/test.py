#!/usr/bin/env python3
import unittest
from tests.request import TestRequestProcessing
from tests.responsebody import TestResponseBodyMaking
from tests.response import TestResponseMaking
from tests.log import TestLogging


if __name__ == "__main__":
	unittest.main()
