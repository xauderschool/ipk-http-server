#!/usr/bin/env python3
import socket
import re
import log
import config


def make_request(host, port, path):
	"""
	Makes a HTTP GET request and recieves response. Only this method should
	be called from other modules

	Param host: HTTP host - this server will be requested
	Param port: Server port on host system (usually 80)
	Param path: Requested path

	Returns: headers, body string. Both are empty if client cannot connect
	  to given host and port
	"""
	in_log = log.Log(config.CLIENT_LOG_BASE, config.LOG_SUFFIX_IN)
	out_log = log.Log(config.CLIENT_LOG_BASE, config.LOG_SUFFIX_OUT)

	try:
		client_socket = init_socket(host, port)
		send_request(client_socket, path, host, out_log)
		headers, response_body = get_response(client_socket, in_log)
		return headers, response_body
	except OverflowError:
		print("Port number must be between 0 and 65535")
	except ConnectionRefusedError:
		print("Could not connect to the given host")
	except socket.gaierror:
		print("Could not resolve name %s" % host)
	return "", ""


def init_socket(host, port):
	"""
	Creates connection for sending HTTP messages

	Param host: HTTP host - this server will be requested
	Param port: Server port on host system (usually 80)

	Returns: socket for communcation with HTTP server
	"""
	client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	client_socket.connect((host, port))
	return client_socket


def send_request(client_socket, path, host, out_log):
	"""
	Sends HTTP GET request to the server

	Param client_socket: socket for communcation with HTTP server
	Param path: Requested path
	Param host: For HTTP Host header
	Param out_log: Log object for logging outgoing messages

	Returns: None
	"""
	request_text = "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n" % (path, host)
	request_bytes = bytes(request_text, "utf-8")
	out_log.write_message(request_bytes)
	client_socket.send(request_bytes)


def get_response(client_socket, in_log):
	"""
	Gets response to the HTTP GET request

	Param client_socket: socket for communcation with HTTP server
	Param in_log: Log object for logging incomming messages

	Returns headers, body string
	"""
	data_size = 1024
	header_bytes = client_socket.recv(data_size)
	in_log.write_message(header_bytes)
	header_str = header_bytes.decode("utf-8")

	chunked = header_str.find("Transfer-Encoding: chunked")

	if chunked >= 0: 
		response_body_str = recieve_chunked(client_socket, in_log)
	else:
		length_match = re.search(r'Content-Length\:\ ([0-9]+)', header_str)
		length = int(length_match.group(1))
		response_body = client_socket.recv(length)
		in_log.write_message(response_body)
		response_body_str = response_body.decode("utf-8")

	return header_str, response_body_str


def recieve_chunked(client_socket, in_log):
	"""
	Recieves all chunks and reconstruct the body from them if server uses
	chunked transfer encoding

	Param client_socket: socket for communcation with HTTP server
	Param in_log: Log object for logging incomming messages

	Returns: response body string
	"""
	recieving_chunk_size = True
	data_size = 1
	chunk_size_str = ""
	response = ""

	while True:
		data = client_socket.recv(data_size)
		in_log.write_message(data)

		if recieving_chunk_size:
			chunk_size_str += data.decode("utf-8")
			if chunk_size_str[-2:] == "\r\n":
				recieving_chunk_size = False
				data_size = int(chunk_size_str[:-2], 16)
				chunk_size_str = ""

		else:
			response += data.decode("utf-8")
			client_socket.recv(2)
			recieving_chunk_size = True
			data_size = 1

		if data_size == 0:
			break	

	client_socket.recv(1)
	client_socket.close()
	return response
